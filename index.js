class Product {
  constructor(name, price) {
    this.name = name;
    this.price = price;
    this.isActive = true;
  }
  archive() {
    this.isActive = false;
    return this;
  }
  updatePrice(newPrice) {
    this.price = newPrice;
    return this;
  }
  unarchive() {
    this.isActive = true;
    return this;
  }
}

class Cart {
  constructor() {
    this.contents = [];
    this.totalAmount = 0;
  }
  addToCart(product, quantity) {
    if (product.isActive === true) {
      this.contents.push({
        product: product,
        quantity: quantity,
      });
      this.computeTotal();
    } else {
      console.log("Product is no longer available");
    }
    return this;
  }
  showCartContents() {
    console.log(this.contents);
    return this;
  }
  updateProductQuantity(prodName, newQuantity) {
    // try {
    let indexofProd = -1;
    let matchProduct = this.contents.filter((prod, index) => {
      indexofProd = index;
      return prod.product.name === prodName;
    });
    matchProduct[indexofProd].quantity = newQuantity;
    // } catch {
    //   console.log("Cannot find product!");
    // }
    return this;
  }
  clearCartContents() {
    this.contents.splice(0, this.contents.length);
    this.totalAmount = 0;
    return this;
  }
  computeTotal() {
    let amount = 0;
    this.contents.forEach((prod) => {
      amount += prod.product.price * prod.quantity;
    });
    this.totalAmount = amount;
    return this;
  }
}

class Costumer {
  constructor(email) {
    this.email = email;
    this.cart = new Cart();
    this.orders = [];
  }
  checkOut() {
    if (this.cart.length !== 0) {
      const orderedProduct = JSON.parse(JSON.stringify(this.cart.contents));
      let order = {
        products: orderedProduct,
        totalAmount: this.cart.totalAmount,
      };
      this.orders.push(order);
      this.cart.clearCartContents();
      console.log("Order successfully placed.");
      return this;
    }
  }
}

const john = new Costumer("john@mail.com");
const prodA = new Product("soap", 9.99);
